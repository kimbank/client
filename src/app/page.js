//해당 페이지는 그냥 보여드리려고 만든거라 지우셔도 됩니다.

'use client'

import Container from '@mui/material/Container';
import Header from './components/Header';
import { EditButton, ListButton, MainButton, MainHalfButton, SubButton, SubHalfButton, SubMiniButton, UploadButton } from './components/Button';
import { DropdownInput, LongText, TextInput, Range, TextField } from './components/Input';
import { CancelCheckbox, CheckedCheckbox, DefaultCheckbox } from './components/Checkbox';
import { Toggle } from './components/Toggle';
import { SuccessNotification, DangerNotification, InfoText, DangerMiniNotification, Certification } from './components/Notification';
import ProgressBar from './components/Notification';
import { StepsToggle, RatingToggle } from './components/Steps';
import SwipeableEdgeDrawer from './components/Popup';
import Footer from './components/Footer';
import { ThemeProvider } from '@emotion/react';
import theme from './theme'

const buttonTitle = '제목'

export default function Home() {
  return (
    <Container>
      <ThemeProvider theme={theme}>
        <Header />
        <Container sx={{
          marginTop: '100px',
          marginBottom: '100px',
          display: 'grid',
          gridTemplateColumns: 'repeat(1, minmax(200px, 1fr))',
          gap: 2
        }}>
          <h1>다음은 버튼입니다.</h1>
          <MainButton buttonName={buttonTitle}/>
          <SubButton buttonName={buttonTitle}/>
          <MainHalfButton buttonName={buttonTitle}/>
          <SubHalfButton buttonName={buttonTitle}/>
          <EditButton buttonName={buttonTitle}/>
          <UploadButton buttonName={buttonTitle}/>
          <ListButton buttonName={buttonTitle}/>
          <SubMiniButton buttonName={buttonTitle}/>
          <h1>다음은 입력창입니다.</h1>
          <DropdownInput buttonName={buttonTitle}/>
          <TextInput buttonName={buttonTitle}/>
          <LongText buttonName={buttonTitle}/>
          <Range buttonName={buttonTitle}/>
          <h1>다음은 체크박스입니다.</h1>
          <DefaultCheckbox buttonName={buttonTitle}/>
          <CheckedCheckbox buttonName={buttonTitle}/>
          <CancelCheckbox buttonName={buttonTitle}/>
          <CancelCheckbox buttonName={buttonTitle}/>
          <h1>다음은 토글입니다.</h1>
          <Toggle />
          <h1>다음은 알림입니다.</h1>
          <SuccessNotification />
          <DangerNotification />
          <InfoText />
          <DangerMiniNotification />
          <Certification />
          <h1>다음은 단계입니다.</h1>
          <StepsToggle />
          <RatingToggle />
          <SwipeableEdgeDrawer />
          <Footer />
        </Container>
      </ThemeProvider>
    </Container>

  )
}

