import { createTheme } from "@mui/material";

// 여기는 컬러 베리에이션입니다.
// 메인컬러는 주황색인 #FF7700, 서브컬러는 회색계열인 #F7F4F2입니다.
// MUI에서는 컴포넌트 속 color 프롭으로 primary/secondary/error/info/inherit/success/warning으로 나뉘어집니다.
    // 해당 작업에서는 primary/secondary만 생성해두었습니다.
// MUI에서는 각각의 다른 디자인을 모두 수정할 수 있습니다.
    // 예를 들어서, ToggleButton의 &.Mui-selected는 버튼이 클릭되었을 때의 UI를 지시합니다.

const theme = createTheme({
    palette: {
      primary: {
        light: '#FFA266',
        main: '#FF7700',
        dark: '#C45A00',
        contrastText: '#fff',
      },
      secondary: {
        light: '#FFFFFF',
        main: '#F7F4F2',
        dark: '#B2B0AE',
        contrastText: '#3C3B3A',
      },
    },
  });

export default theme