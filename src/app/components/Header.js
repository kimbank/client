import AppBar from '@mui/material/AppBar';
import { Toolbar, Typography } from '@mui/material';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import TollOutlinedIcon from '@mui/icons-material/TollOutlined';
import { Container, Box } from '@mui/material';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';

const credit = 100

// Figma: Header
export default function Header() {
    return (
        <AppBar position='fixed' sx={{
            backgroundColor: '#FFFFFF', height: 64,
            display: 'flex', flexDirection: 'row', justifyContent: 'space-between', padding: '0 24px', width: '100%'
        }}>
            <Container disableGutters sx={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center'
            }}>
                <Toolbar sx={{ gap: 0.5, flexGrow: 1, marginTop: '6px' }}>
                    <img src="logo.png" alt="logo" width={19.5} height={32} />
                    <div className='logo-title' style={{ marginTop: '-12px' }}>온리유</div>
                </Toolbar>

                <Box sx={{ display: 'flex', gap: 2 }}>
                    <Box sx={{ display: 'flex', gap: '4px', alignItems: 'center' }}>
                        <Typography className='credit' sx={{ color: '#FF7700' }} >{ credit }</Typography>
                        <TollOutlinedIcon sx={{ color: '#FF7700' }} />
                    </Box>
                    <NotificationsNoneIcon sx={{ color: '#FF7700' }} />
                    <HelpOutlineIcon sx={{ color: '#FF7700' }} />
                </Box>

            </Container>
        </AppBar>
    )
}
