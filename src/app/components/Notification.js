'use client'

import { Alert } from "@mui/material";
import { Button, Typography } from "@mui/material";
import InfoIcon from '@mui/icons-material/Info';
import WarningIcon from '@mui/icons-material/Warning';
import CheckIcon from '@mui/icons-material/Check';
import * as React from 'react';
import { useState, useEffect } from "react";
import { Container } from "@mui/material";


//Figma: Success notification
export function SuccessNotification({ alertMessage, visible, setVisible /* visible, setVisible useState를 page.js에서도 호출하기 위해 export */ }) {
    const [notificationHeight, setNotificationHeight] = useState(0); // 시작 시 위치
    const time = 2000; // notification이 내려왔을 때 대기하는 시간인데, 변경해도 됩니다
    const drop = () => {
        setNotificationHeight(128);
        setTimeout(() => setNotificationHeight(0), time);
        setTimeout(() => setVisible(false), time + 500)
    };

    useEffect(() => {
        if (visible) {
            drop();
        }
    }, [visible]);

    return (
        <Container disableGutters>
            {visible && (
                <Alert severity="success" sx={{
                    borderRadius: 2,
                    position: 'fixed',
                    top: `${notificationHeight}px`,
                    left: '24px',
                    right: '24px',
                    transition: 'top 0.5s ease-in-out',
                }}>
                    {alertMessage}
                </Alert>
            )}
        </Container>
    );
}


//Figma: Danger notification
export function DangerNotification({ alertMessage, visible, setVisible /* visible, setVisible useState를 page.js에서도 호출하기 위해 export */ }) {
    const [notificationHeight, setNotificationHeight] = useState(0); // 시작 시 위치
    const time = 2000; // notification이 내려왔을 때 대기하는 시간인데, 변경해도 됩니다
    const drop = () => {
        setNotificationHeight(128);
        setTimeout(() => setNotificationHeight(0), time);
        setTimeout(() => setVisible(false), time + 500)
    };

    useEffect(() => {
        if (visible) {
            drop();
        }
    }, [visible]);

    return (
        <Container disableGutters>
            {visible && (
                <Alert severity="error" sx={{
                    borderRadius: 2,
                    position: 'fixed',
                    top: `${notificationHeight}px`,
                    left: '24px',
                    right: '24px',
                    transition: 'top 0.5s ease-in-out',
                }}>
                    {alertMessage}
                </Alert>
            )}
        </Container>
    );
}



// Figma: Info text
// 이 버튼은 위 notification과 달리, 나왔다가 들어갔다 하는 컴포넌트가 아니라서 애니메이션이 없습니다.
export function InfoText({ alertMessage }) {
    return (
        <div>
            <Button
                variant='contained'
                sx={{
                    borderRadius: '8px',
                    height: '33px',
                    padding: '8px 12px',
                    backgroundColor: '#F7F4F2',
                    display: 'flex',
                    alignItems: 'center'
                }}
            >
                <InfoIcon sx={{
                    marginRight: 1,
                    color: '#FF7700'
                }} />
                <Typography
                    variant='subtitle1'
                    className="caption"
                    sx={{
                        color: '#3C3B3A'
                    }}
                >
                    {alertMessage}
                </Typography>
            </Button>
        </div>
    )
}




// Figma: Danger mini notification
// 이 버튼은 위 notification과 달리, 나왔다가 들어갔다 하는 컴포넌트가 아니라서 애니메이션이 없습니다.
export function DangerMiniNotification({ alertMessage }) {
    return (
        <div>
            <Button
                variant='contained'
                sx={{
                    borderRadius: '8px',
                    height: '33px',
                    padding: '8px 12px',
                    backgroundColor: '#F7F4F2',
                    display: 'flex',
                    alignItems: 'center'
                }}
            >
                <WarningIcon sx={{
                    marginRight: 1,
                    color: '#FF8982'
                }} />
                <Typography
                    variant='subtitle1'
                    className="caption"
                    sx={{
                        color: '#FF8982'
                    }}
                >
                    {alertMessage}
                </Typography>
            </Button>
        </div>
    )
}




// Figma: Certification
// 인증마크입니다.
// 이 버튼은 위 notification과 달리, 나왔다가 들어갔다 하는 컴포넌트가 아니라서 애니메이션이 없습니다.
export function Certification({ alertMessage }) {
    return (
        <div>
            <Button
                variant='contained'
                sx={{
                    borderRadius: '8px',
                    height: '33px',
                    padding: '8px 12px',
                    backgroundColor: '#F7F4F2',
                    display: 'flex',
                    alignItems: 'center'
                }}
            >
                <CheckIcon sx={{
                    marginRight: 1,
                    color: '#FF7700'
                }} />
                <Typography
                    variant='subtitle1'
                    className="caption"
                    sx={{
                        color: '#FF7700'
                    }}
                >
                    {alertMessage}
                </Typography>
            </Button>
        </div>
    )
}
